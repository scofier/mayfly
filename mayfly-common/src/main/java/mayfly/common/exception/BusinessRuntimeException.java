package mayfly.common.exception;

/**
 * @author meilin.huang
 * @version 1.0
 * @date 2019-01-05 2:19 PM
 */
public class BusinessRuntimeException extends RuntimeException {

    public BusinessRuntimeException(String msg) {
        super(msg);
    }
}
