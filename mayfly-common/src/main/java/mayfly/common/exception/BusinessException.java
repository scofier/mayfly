package mayfly.common.exception;

/**
 * @author meilin.huang
 * @version 1.0
 * @date 2018-12-07 3:10 PM
 */
public class BusinessException extends Exception {
    public BusinessException(String msg) {
        super(msg);
    }
}
