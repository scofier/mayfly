package mayfly.common.enums;

/**
 * 最简单的枚举类，即只含value的枚举
 * @author meilin.huang
 * @version 1.0
 * @date 2019-04-22 10:10
 */
public interface ValueEnum {
    /**
     * 获取枚举值
     * @return  枚举值
     */
    Integer getValue();
}
