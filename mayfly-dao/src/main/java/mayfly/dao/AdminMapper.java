package mayfly.dao;

import mayfly.dao.base.BaseMapper;
import mayfly.entity.Admin;

/**
 * @Description: 管理员Mapper
 * @author: hml
 * @date: 2018/6/27 下午2:35
 */
public interface AdminMapper extends BaseMapper<Admin> {
}
