package mayfly.dao;

import mayfly.dao.base.BaseMapper;
import mayfly.entity.Redis;

/**
 * @author meilin.huang
 * @version 1.0
 * @date 2019-01-07 4:07 PM
 */
public interface RedisMapper extends BaseMapper<Redis> {
}
