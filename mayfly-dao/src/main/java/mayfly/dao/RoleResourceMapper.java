package mayfly.dao;

import mayfly.dao.base.BaseMapper;
import mayfly.entity.RoleResource;

/**
 * @author meilin.huang
 * @version 1.0
 * @date 2018-12-07 4:15 PM
 */
public interface RoleResourceMapper extends BaseMapper<RoleResource> {
}
