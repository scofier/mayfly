package mayfly.dao;

import mayfly.dao.base.BaseMapper;
import mayfly.entity.PermissionGroup;

/**
 * @author meilin.huang
 * @version 1.0
 * @date 2019-03-26 2:52 PM
 */
public interface PermissionGroupMapper extends BaseMapper<PermissionGroup> {
}
