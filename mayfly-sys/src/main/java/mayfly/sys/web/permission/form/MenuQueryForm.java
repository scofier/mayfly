package mayfly.sys.web.permission.form;

import lombok.Data;

/**
 * @author meilin.huang
 * @version 1.0
 * @date 2018-12-20 10:09 AM
 */
@Data
public class MenuQueryForm {

    private Byte status;
}
