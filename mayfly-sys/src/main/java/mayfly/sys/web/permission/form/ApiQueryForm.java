package mayfly.sys.web.permission.form;

import lombok.Data;

/**
 * @author meilin.huang
 * @version 1.0
 * @date 2018-11-23 5:59 PM
 */
@Data
public class ApiQueryForm {

    private String description;
}
