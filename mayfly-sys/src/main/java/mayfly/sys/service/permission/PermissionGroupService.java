package mayfly.sys.service.permission;

import mayfly.entity.PermissionGroup;
import mayfly.sys.service.base.BaseService;

/**
 * @author meilin.huang
 * @version 1.0
 * @date 2019-03-26 2:53 PM
 */
public interface PermissionGroupService extends BaseService<PermissionGroup> {
}
